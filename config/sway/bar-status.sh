#! /usr/bin/env bash

while true
do

  datestr=$(date +'%Y-%m-%d %H:%M:%S')
  batstr=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep percentage | awk '{print $2}')

  echo "🔋 $batstr | ⏲  $datestr"

  sleep 1
done
