#! /bin/bash

cp -v $HOME/.bashrc config/bashrc
cp -v $HOME/.profile config/profile
cp -v $HOME/.config/nvim/init.vim config/vimrc
cp -v $HOME/.gitconfig config/gitconfig

cp -v $HOME/.tmux.conf config/tmux.conf
