#! /usr/bin/env python
""" Create a backup, directed by a YAML config file

The config must have keys:

    - backup_directory

with optional keys:

    - includes (list of paths)
    - excludes (list of paths)
    - post_args (list of duplicity flags)

Requires:
    duplicity
    pyyaml package
"""

import sys
import subprocess
import yaml
import argparse
from os.path import expanduser, isdir

parser = argparse.ArgumentParser()
parser.add_argument("config_file")
parser.add_argument("-f", "--full", action="store_true")
parser.add_argument("--dryrun", action="store_true")
args = parser.parse_args()

# Load the configuration
if args.config_file.endswith(".yaml"):
    with open(args.config_file, "r") as f:
        config = yaml.load(f.read())
else:
    raise IOError("Configuration file must be YAML")

# Parse options and call duplicity
cmd = ["duplicity"]

if args.full:
    cmd.append("full")

for arg in config.get("post_args", "").split():
    cmd.append(arg)

if len(config.get("archive-dir", "")) != 0:
    cmd.extend(["--archive-dir", config["archive-dir"]])

for path in config.get("excludes", "").split():
    cmd.extend(["--exclude", expanduser(path)])

for path in config.get("includes", "").split():
    cmd.extend(["--include", expanduser(path)])

target = config.get("backup_target", "~")
cmd.append(expanduser(target))

cmd.append(config["backup_directory"])
if args.dryrun:
    print(" ".join(cmd))
else:
    subprocess.call(cmd)

